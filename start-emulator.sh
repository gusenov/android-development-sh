#!/bin/bash
#set -x  # echo on


android_sdk_location="/opt/google/Android/Sdk"
avd_name="Nexus_5X_API_28_x86"

rm $HOME/.android/avd/$avd_name.avd/*.lock

# $ $android_sdk_location/emulator/emulator -help
# Android Emulator usage: emulator [options] [-qemu args]
#   options:
#     -list-avds                      list available AVDs
#     -no-cache                       disable the cache partition
#     -nocache                        same as -no-cache
#     -no-snapstorage                 do not mount a snapshot storage file (this disables all snapshot functionality)
#     -no-snapshot                           perform a full boot and do not auto-save, but qemu vmload and vmsave operate on snapstorage
#     -no-snapshot-save                      do not auto-save to snapshot on exit: abandon changed state
#     -no-snapshot-load                      do not auto-start from snapshot: perform a full boot
#     -snapshot-list                         show a list of available snapshots
#     -no-snapshot-update-time               do not try to correct snapshot time on restore
#     -wipe-data                      reset the user data image (copy it from initdata)
#     -avd <name>                     use a specific android virtual device
#     -accel <mode>                   Configure emulation acceleration
#     -ranchu                         Use new emulator backend instead of the classic one
#     -engine <engine>                Select engine. auto|classic|qemu2
#     -no-jni                         disable JNI checks in the Dalvik runtime
#     -nojni                          same as -no-jni
#     -use-system-libs                Use system libstdc++ instead of bundled one
#     -no-audio                       disable audio support
#     -noaudio                        same as -no-audio
#     -no-boot-anim                   disable animation for faster boot
#     -no-sim                         device has no SIM card
#     -version                        display emulator version number
#     -no-passive-gps                 disable passive gps updates
#     -gpu <mode>                     set hardware OpenGLES emulation mode
#
#      -qemu args...                  pass arguments to qemu
# $ $android_sdk_location/emulator/emulator -help-accel

$android_sdk_location/emulator/emulator -list-avds
# Nexus_5X_API_27_x86

$android_sdk_location/emulator/emulator -snapshot-list -avd "$avd_name"

$android_sdk_location/emulator/emulator \
    -no-cache -nocache \
    -no-snapstorage -no-snapshot -no-snapshot-save -no-snapshot-load -no-snapshot-update-time \
    -wipe-data \
    -avd "$avd_name" \
    -accel on \
    -ranchu \
    -engine qemu2 \
    -no-jni -nojni \
    -use-system-libs \
    -no-audio -noaudio \
    -no-boot-anim \
    -no-sim \
    -version \
    -no-passive-gps \
    -gpu on \
    -qemu \
        -m 2047 \
        -enable-kvm

